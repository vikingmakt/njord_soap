const ExecuteService = require('./execute');
const Options = require('./options');
const RabbitMQ = require('amqp-node');

function init () {
    RabbitMQ.engineOpen(
        Options.rabbitmqName,
        {
            url: Options.rabbitmqUrl
        }
    );
    process.nextTick(services);
}

function serviceExecute () {
    if (Options.services && Options.services.execute) {
        if (Options.njordSoapEnv instanceof Array) {
            return Options.njordSoapEnv.forEach(function (env) {
                ExecuteService(Options.services.execute,env);
            });
        }
        ExecuteService(Options.services.execute,Options.njordSoapEnv);
    }
}

function services () {
    process.nextTick(serviceExecute);
}

init();
