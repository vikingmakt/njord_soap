var Task = require('../../../execute/task');
var test = require('tape');

test('Task:getMethod', function (t) {
    var client = {
        testMethod: function () {}
    };
    var msg = {
        body: {
            method: 'testMethod'
        }
    };
    var backup = Task.invokeMethod;
    Task.invokeMethod = function (method, _msg) {
        t.notEqual(method,undefined);
        t.equal(_msg,msg);
        t.end();
        Task.invokeMethod = backup;
    };
    Task.getMethod(client,msg);
});

test('Task:getMethod not found', function (t) {
    var client = {
        testMethod: false
    };
    var backup = Task.broadcast;
    Task.broadcast = function (channel, headers, body) {};
    var msg = {
        body: {
            method: 'testMethod'
        },
        properties: {
            headers: {
                etag: '12345'
            }
        },
        ack: function (result) {
            Task.broadcast = backup;
            t.equal(result,true);
            t.end();
        }
    };
    Task.getMethod(client,msg);
});

test('Task:handleResponse', function (t) {
    var expected = 'asdasd';
    var backup = Task.broadcast;
    var backupLog = Task.logTime;
    Task.logTime = function () {};
    Task.broadcast = function (channel, headers, body) {
        Task.broadcast = backup;
        Task.logTime = backupLog;
        t.equal(JSON.parse(body).responseMsg,expected);
        t.end();
    };
    Task.handleResponse({
        ack: function () {},
        body: {
        },
        properties: {
            headers: {}
        }
    }, null, expected);
});

test('Task:handleResponse with err', function (t) {
    var expected = 'asdasd';
    var backup = Task.broadcast;
    var backupLog = Task.logTime;
    Task.logTime = function () {};
    Task.broadcast = function (channel, headers, body) {
        Task.broadcast = backup;
        Task.logTime = backupLog;
        t.equal(JSON.parse(body).errMsg,expected);
        t.end();
    };
    Task.handleResponse({
        ack: function () {},
        body: {
        },
        properties: {
            headers: {}
        }
    }, {message: expected}, null);
});

test('Task:invokeMethod with args', function (t) {
    var msg = {
        body: {
            args: {
                a: true
            }
        }
    };
    Task.invokeMethod(function (args, cb, op) {
        t.equal(args,msg.body.args);
        t.ok(op.timeout,'op.timeout is present');
        t.end();
    },msg);
});

test('Task:invokeMethod with timeout', function (t) {
    var msg = {
        body: {
            timeout: 10
        }
    };
    Task.invokeMethod(function (args, cb, op) {
        t.equal(op.timeout,msg.body.timeout);
        t.end();
    },msg);
});

test('Task:invokeMethod without args', function (t) {
    var msg = {
        body: {
        }
    };
    Task.invokeMethod(function (args, cb, op) {
        t.equal(Object.keys(args).length,0);
        t.ok(op.timeout,'op.timeout is present');
        t.end();
    },msg);
});

test('Task:invokeMethod without timeout', function (t) {
    var msg = {
        body: {
        }
    };
    Task.invokeMethod(function (args, cb, op) {
        t.ok(op.timeout,'op.timeout is present');
        t.end();
    },msg);
});

test('Task:run', function (t) {
    var backup = Task.validate;
    Task.validate = function (msg) {
        Task.validate = backup;
        t.equal(msg.body.a,2);
        t.end();
    };
    Task.run({body:'{"a":2}'});
});

test('Task:run should discart invalid JSON', function (t) {
    Task.run({
        body:'sadad',
        ack: function (result) {
            t.equal(result,true);
            t.end();
        }
    });
});

test('Task:validate with args', function (t) {
    var backup = Task.genClient;

    Task.genClient = function (msg) {
        Task.genClient = backup;
        t.pass('Validate passed');
        t.end();
    }
    Task.validate({
        properties: {
            headers: {
                etag: '12345'
            }
        },
        body: {
            url: 'url',
            method: 'method',
            args: {test:true}
        }
    });
});

test('Task:validate with wrong args', function (t) {
    Task.validate({
        ack: function (result) {
            t.equal(result,true);
            t.end();
        },
        properties: {
            headers: {
                etag: '12345'
            }
        },
        body: {
            url: 'url',
            method: 'method',
            args: true
        }
    });
});

test('Task:validate without args', function (t) {
    var backup = Task.genClient;

    Task.genClient = function (msg) {
        Task.genClient = backup;
        t.pass('Validate passed');
        t.end();
    }
    Task.validate({
        properties: {
            headers: {
                etag: '12345'
            }
        },
        body: {
            url: 'url',
            method: 'method'
        }
    });
});

test('Task:validate should fail without etag', function (t) {
    Task.validate({
        ack: function (result) {
            t.equal(result,true);
            t.end();
        },
        properties: {
            headers: {
            }
        },
        body: {
            url: 'url',
            method: 'method',
            args: {test:true}
        }
    });
});

test('Task:validate should fail without method', function (t) {
    Task.validate({
        ack: function (result) {
            t.equal(result,true);
            t.end();
        },
        properties: {
            headers: {
                etag: '12345'
            }
        },
        body: {
            url: 'url',
            args: {test:true}
        }
    });
});

test('Task:validate should fail without url', function (t) {
    Task.validate({
        ack: function (result) {
            t.equal(result,true);
            t.end();
        },
        properties: {
            headers: {
                etag: '12345'
            }
        },
        body: {
            method: 'method',
            args: {test:true}
        }
    });
});
