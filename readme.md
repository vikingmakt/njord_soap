# Njord SOAP

# Summary

+ [Definitions](#definitions)
+ [Execute](#execute)

## Definitions

## Execute

### Ask

+ exchange: **tornpack**
+ name: **njord_soap_execute**
+ headers:
  + **etag**
+ body: base64

```json
{
    "url": $WSDL_URL,
    "method": $METHOD_NAME,
    "args": {
        $KEY: $VALUE
    },
    "timeout": $REQUEST_TIMEOUT (default = 60 sec)
}
```

### Answer

+ exchange: **tornpack**
+ headers:
  + **etag**
  + **code**
+ body: base64

```json
{
    "responseMsg": $msg
}
```

+ codes:
 + clientError: **7905aff0294f43cdbf941af06d080712**
 + methodError: **690a193bfb724599b24fe855b3929708**
 + methodNotExists: **65fbc13f0699438e8820110cdfa255cd**
 + ok: **1ecd24c960f04003885ae39587657bfc**
