exports.basicAuth = function (user, pass) {
    return 'Basic ' + new Buffer(user + ':' + pass).toString('base64')
};
