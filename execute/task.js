const basicAuth = require('../auth').basicAuth;
const moment = require('moment');
const Options = require('../options');
const Publish = require('amqp-node/publish');
const routingKey = require('amqp-node/static').routingKey;
const Soap = require('soap');

function _decode (body) {
    try {
        return JSON.parse(new Buffer(body.toString(),'base64').toString('utf8'));
    } catch (err) {}
    return false;
}

function _diffTime (start) {
    return moment.utc(moment().diff(start));
}

function _encode (body) {
    return new Buffer(JSON.stringify(body)).toString('base64');
}

function auth (msg) {
    if (msg.body.username && msg.body.password)
        msg.wsdl_headers.Authorization = basicAuth(msg.body.username,msg.body.password);
    return process.nextTick(genClient,msg);
}

function broadcast (channel, headers, body) {
    Publish.push(
        channel,
        Options.tornpackRabbitmqExchange.default,
        '',
        body || '',
        {
            headers: headers
        }
    );
}

function genClient (msg) {
    function onCreate (err, client) {
        if (err)
            return process.nextTick(onClientErr,msg,err);
        return process.nextTick(getMethod,msg,client);
    }
    return Soap.createClient(
        msg.body.url,
        {
            wsdl_headers: msg.wsdl_headers
        },
        onCreate
    );
}

function getMethod (msg, client) {
    var method = client[msg.body.method];
    if (typeof method == 'function')
        return process.nextTick(invokeMethod,method,msg);

    if (Options.fallbackToSuds)
        return process.nextTick(tryWithSuds,msg);

    return process.nextTick(
        respond,
        msg,
        Options.codes.execute.methodNotExists
    );
}

function handleResponse (msg, err, result) {
    var time = _diffTime(msg.requestStart);
    logTime(msg.body.url,msg.body.method,time);

    if (err)
	return process.nextTick(onResponseError,msg,err,time);
    return process.nextTick(onResponseOk,msg,result,time);
}

function invokeMethod (method, msg) {
    msg.requestStart = moment();
    method(
        msg.body.args || {},
        handleResponse.bind(null,msg),
        {
            timeout: msg.body.timeout || Options.invokeTimeout
        }
    );
}

function logTime (url, method, time) {
    console.log('Response from:',url + '/' + method,':: in',time.format('HH:mm.ss:SSS'));
}

function onClientErr (msg, err) {
    if (Options.fallbackToSuds)
        return process.nextTick(tryWithSuds,msg);

    return process.nextTick(
        respond,
        msg,
        Options.codes.execute.clientError,
        _encode({responseMsg: err.message || err})
    );
}

function onResponseError (msg, err, time) {
    if (Options.fallbackToSuds)
        return process.nextTick(tryWithSuds,msg);

    return process.nextTick(
        respond,
        msg,
        Options.codes.execute.methodError,
        _encode({
            responseMsg: err.message,
            time: time.unix()
        })
    );
}

function onResponseOk (msg, result, time) {
    return process.nextTick(
        respond,
        msg,
        Options.codes.execute.ok,
        _encode({
            responseMsg: result,
            time: time.unix()
        })
    );
}

function run (msg) {
    if (!msg.properties.headers || !msg.properties.headers.etag)
	return msg.ack(true);
    msg.body = _decode(msg.body);
    if (!msg.body)
	return msg.ack(true);
    msg.wsdl_headers = {};
    return process.nextTick(validate,msg);
}

function respond (msg, code, body) {
    delete msg.properties.headers.__ask__;
    msg.properties.headers.code = code;
    msg.properties.headers.response = true;
    broadcast(
	msg.channel,
	msg.properties.headers,
	body
    );
    return msg.ack(true);
}

function tryWithSuds (msg) {
    Publish.push(
        msg.channel,
        Options.tornpackRabbitmqExchange.default,
        routingKey(Options.services.sudsExecute),
        _encode(msg.body),
        {
            headers: msg.properties.headers
        }
    );
    return msg.ack(true);
}

function validate (msg) {
    if (!msg.properties.headers.etag)
        return msg.ack(true);
    if (!msg.body.url)
        return msg.ack(true);
    if (!msg.body.method)
        return msg.ack(true);
    if (msg.body.args !== undefined && !(msg.body.args instanceof Object))
        return msg.ack(true);
    return process.nextTick(auth,msg);
}

module.exports = run;
