const channelName = require('amqp-node/static').channelName;
const queueName = require('amqp-node/static').queueName;
const Options = require('../options');
const RabbitMQ = require('amqp-node');
const routingKey = require('amqp-node/static').routingKey;
const Task = require('./task');
const uuid4 = require('uuid4');

function run (name, env) {
    var _channel = null;
    var _engine = null;
    var _queue = queueName([name,env,Options.serviceName].join('_'));
    var _uid = uuid4();

    function call (msg) {
        function ack (result) {
            if (result)
                return _channel.ack(msg);
            return _channel.nack(msg);
        }

        Task({
            body: msg.content,
            channel: _channel,
            properties: msg.properties,
            ack: ack
        });
    }

    function consumer () {
        _engine.consumer.consume(
            _channel,
            _queue,
            call
        );
    }

    function exchangeDeclare () {
        _engine.exchange.declare(
            _channel,
            Options.tornpackRabbitmqExchange.default,
            'topic',
            {},
            onExchangeDeclare
        );
    }

    function exchangeHeadersAnyBind () {
        _engine.exchange.bind(
            _channel,
            Options.tornpackRabbitmqExchange.headers,
            Options.tornpackRabbitmqExchange.default,
            '#'
        );
    }

    function exchangeHeadersAnyDeclare () {
        _engine.exchange.declare(
            _channel,
            Options.tornpackRabbitmqExchange.headersAny,
            'headers',
            {
                alternateExchange: Options.tornpackRabbitmqExchange.headersAnyAe
            },
            onExchangeHeadersAnyDeclare
        );
    }

    function onChannel (channel) {
        _channel = channel;
        process.nextTick(queueDeclare);
        process.nextTick(exchangeDeclare);
    }

    function onEngine (engine) {
        _engine = engine;
        _engine.channel(
            channelName(Options.rabbitmqChannel,_uid),
            {
                prefetch: Options.rabbitmqPrefetch
            },
            onChannel
        );
    }

    function onExchangeDeclare () {
        return process.nextTick(exchangeHeadersAnyDeclare);
    }

    function onExchangeHeadersAnyDeclare () {
        return process.nextTick(exchangeHeadersAnyBind);
    }

    function queueBind () {
        _engine.queue.bind(
            _channel,
            _queue,
            Options.tornpackRabbitmqExchange.default,
            routingKey(name)
        );
        _engine.queue.bind(
            _channel,
            _queue,
            Options.tornpackRabbitmqExchange.headersAny,
            '',
            {
                soap_env: env
            }
        );
    }

    function queueDeclare () {
        _engine.queue.declare(
            _channel,
            _queue,
            {
                durable: true
            },
            function () {
                process.nextTick(queueBind);
                process.nextTick(consumer);
            }
        );
    }

    RabbitMQ.engine(
        Options.rabbitmqName,
        onEngine
    );
}

module.exports = run;
