from base64 import b64decode,b64encode
from datetime import datetime
from pika import BasicProperties
from suds import PortNotFound
from suds.sudsobject import asdict
from suds.client import Client
from tornpack.actor.rabbitmq import SimpleConsumer
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify
from tornpack.raven import Raven

__all__ = ['Execute']

class Execute(SimpleConsumer):
    def broadcast(self,headers,body=''):
        try:
            assert body
            body = b64encode(jsonify(body))
        except AssertionError:
            pass
        except TypeError:
            raise
        except:
            raise

        self.rabbitmq_channel.basic_publish(
            body=body,
            exchange=options.tornpack_rabbitmq_exchange['default'],
            routing_key='',
            properties=BasicProperties(headers=headers)
        )
        msg['ack'].set_result(True)
        return True

    def gen_client(self,msg):
        try:
            msg['swap']['client'] = Client(
                msg['body']['url'],
                timeout=msg['body'].get('timeout',options.invoke_timeout) / 1000.0,
                username=msg['body'].get('username'),
                password=msg['body'].get('password')
            )
        except Exception as ex:
            self.ioengine.ioloop.add_callback(
                self.broadcast,
                body={
                    'responseMsg':ex.__repr__()
                },
                headers={
                    'code':options.njord_soap_suds['codes']['execute']['client_error'],
                    'etag':msg['properties'].headers['etag']
                }
            )
        else:
            self.ioengine.ioloop.add_callback(self.get_method,msg=msg)
        return True

    def get_method(self,msg):
        try:
            msg['swap']['method'] = getattr(msg['swap']['client'].service,msg['body']['method'])
        except PortNotFound:
            self.ioengine.ioloop.add_callback(
                self.broadcast,
                headers={
                    'code':options.njord_soap_suds['codes']['execute']['method_not_exists'],
                    'etag':msg['properties'].headers['etag']
                }
            )
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.invoke_method,msg=msg)
        return True

    def invoke_method(self,msg):
        start = datetime.utcnow()
        try:
            result = msg['swap']['method'](**msg['body'].get('args',{}))
        except Exception as ex:
            time = datetime.utcnow() - start
            self.ioengine.ioloop.add_callback(
                self.broadcast,
                body={
                    'responseMsg':ex.__repr__(),
                    'time':time.total_seconds()
                },
                headers={
                    'code':options.njord_soap_suds['codes']['execute']['method_error'],
                    'etag':msg['properties'].headers['etag']
                }
            )
        else:
            time = datetime.utcnow() - start
            self.ioengine.ioloop.add_callback(self.parse_result,msg=msg,result=result,time=time)

        Raven.info('Response from: %s/%s :: in %s' % (msg['body']['url'],msg['body']['method'],time))
        return True

    def on_message(self,msg):
        try:
            assert msg['properties'].headers['etag']
            msg['body'] = dictfy(b64decode(msg['body']))
            assert msg['body']
            assert msg['body']['url']
            assert msg['body']['method']
            assert 'args' not in msg['body'] or isinstance(msg['body']['args'],dict)
        except (AssertionError,KeyError):
            msg['ack'].set_result(True)
        except:
            raise
        else:
            msg['swap'] = {}
            self.ioengine.ioloop.add_callback(self.gen_client,msg=msg)
        return True

    def parse_result(self,msg,result,time):
        self.ioengine.ioloop.add_callback(
            self.broadcast,
            body={
                'responseMsg':self.recursive_parse(result),
                'time':time.total_seconds()
            },
            headers={
                'code':options.njord_soap_suds['codes']['execute']['ok'],
                'etag':msg['properties'].headers['etag']
            }
        )
        return True

    def recursive_parse(self,data):
        result = {}
        for k,v in asdict(data).iteritems():
            if hasattr(v,'__keylist__'):
                result[k] = self.recursive_parse(v)
            elif isinstance(v,list):
                result[k] = []
                for item in v:
                    if hasattr(item,'__keylist__'):
                        result[k].append(self.recursive_parse(item))
                    else:
                        result[k].append(item)
            else:
                result[k] = v
        return result
