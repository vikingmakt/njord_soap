from njord_soap.suds_server.execute import Execute
from tornpack.options import options

__all__ = ['Setup']

class Setup(Execute):
    def __init__(self):
        self.ioengine.ioloop.add_callback(self.service_execute)

    def service_execute(self):
        try:
            assert options.njord_soap_suds['services']['execute']
        except:
            pass
        else:
            Execute(options.njord_soap_suds['services']['execute'],prefetch_count=1)
        return True

Setup()
